One of the ways to use it:
```
docker run -v `pwd`/output/:/tmp/yay/ yay -S --noconfirm gomuks-git
```

It's going to pack all the built software it `output` directory which is created in your working(current) directory.
Every package is in it's corresponding directory.
It's hidden in a bulk of stuff used to build the package.
Look for `*.pkg.tar.xz`.


**TODO:**

* Get rid of bulk after build
* Don't actually install packages in container (probably)
