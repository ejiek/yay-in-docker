FROM archlinux/base AS build
RUN pacman -Sy --noconfirm go git sudo base-devel
RUN useradd -m builder
WORKDIR /home/builder
RUN sudo -u builder git clone https://aur.archlinux.org/yay.git
WORKDIR /home/builder/yay
RUN sudo -u builder makepkg
RUN ls -lA

FROM archlinux/base
RUN useradd -m builder
RUN echo builder ALL=NOPASSWD: ALL >> /etc/sudoers
COPY --from=build /home/builder/yay/yay*.pkg.tar.xz /tmp/yay.pkg.tar.xz
RUN pacman -Sy --noconfirm git sudo base-devel
RUN pacman -U --noconfirm /tmp/yay.pkg.tar.xz

USER builder
ENTRYPOINT ["yay","--answerclean","All","--answerdiff","None","--answeredit","None","--builddir","/tmp/yay"]
CMD ["--help"]
